const path = require('path');

export const CSSModules = false;

export const eslint = false;
export const stylelint = true;

export const appSettings = {
  id: 'root',
  title: '',
};

export const paths = {
  client: path.join(__dirname, '../../src/client.js'),
  build: path.join(__dirname, '../../build'),
};

export const vendor = [
  'react',
  'react-dom',
  'redux',
  'react-redux',
  'redux-thunk',
  'axios',
  'lodash',
];
