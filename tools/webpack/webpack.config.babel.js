import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import StyleLintPlugin from 'stylelint-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import template from 'html-webpack-template';

import { paths, vendor, appSettings, CSSModules, stylelint, eslint } from './config';

const nodeEnv = process.env.NODE_ENV || 'development';
const isDev = nodeEnv !== 'production';

const getPlugins = () => {
  const plugins = [
    new HtmlWebpackPlugin({
      title: appSettings.title,
      template,
      appMountId: appSettings.id,
      inject: false,
      mobile: true,
      meta: [
        {
          name: 'description',
          content: '',
        },
      ],
    }),
    new StyleLintPlugin({ syntax: 'scss', failOnError: stylelint }),
    new webpack.EnvironmentPlugin({ NODE_ENV: JSON.stringify(nodeEnv) }),
    new ExtractTextPlugin({
      filename: '[name].[contenthash:8].css',
      allChunks: true,
      disable: isDev,   // Disable css extracting on development
      ignoreOrder: CSSModules,
    }),
  ];

  if (isDev) {
    // Push development plugins
  } else {
    plugins.push(
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        minChunks: Infinity,
      }),
      new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        beautify: false,
        mangle: { screw_ie8: true },
        compress: {
          screw_ie8: true,  // React doesn't support IE8
          warnings: false,
          unused: true,
          dead_code: true,
        },
        output: { screw_ie8: true, comments: false },
      })  // eslint-disable-line comma-dangle
    );
  }

  return plugins;
};

const getEntry = () => {
  let entry = [paths.client];

  if (!isDev) {
    entry = {
      main: paths.client,
      vendor,
    };
  }

  return entry;
};

export default {
  entry: getEntry(),
  output: {
    filename: isDev ? '[name].js' : '[name].[chunkhash:8].js',
    path: paths.build,
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel',
          options: {
            cacheDirectory: isDev,
          },
        },
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style',
          use: [
            {
              loader: 'css',
              options: {
                importLoaders: 1,
                sourceMap: true,
                modules: CSSModules,
                // "context" and "localIdentName" need to be the same with server config,
                // or the style will flick when page first loaded
                context: path.join(process.cwd(), './src'),
                localIdentName: isDev ? '[name]__[local].[hash:base64:5]' : '[hash:base64:5]',
                minimize: !isDev,
              },
            },
            { loader: 'postcss', options: { sourceMap: true } },
          ],
        }),
      },
      {
        test: /\.(scss|sass)$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style',
          use: [
            {
              loader: 'css',
              options: {
                importLoaders: 2,
                sourceMap: true,
                modules: CSSModules,
                context: path.join(process.cwd(), './src'),
                localIdentName: isDev ? '[name]__[local].[hash:base64:5]' : '[hash:base64:5]',
                minimize: !isDev,
              },
            },
            { loader: 'postcss', options: { sourceMap: true } },
            {
              loader: 'sass',
              options: {
                outputStyle: 'expanded',
                sourceMap: true,
                sourceMapContents: !isDev,
              },
            },
          ],
        }),
      },
      {
        test: /\.(woff2?|ttf|eot|svg)$/,
        use: {
          loader: 'url',
          options: { limit: 10000 },
        },
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        use: [
          {
            loader: 'url',
            options: { limit: 10240 },
          },
          {
            loader: 'image-webpack',
            options: { bypassOnDebug: true },
          },
        ],
      },
    ],
  },
  devServer: {
    contentBase: paths.build,
    compress: true,
    port: process.env.PORT,
    host: process.env.host,
  },
  devtool: isDev ? 'source-map' : 'hidden-source-map',
  plugins: getPlugins(),
  resolveLoader: {
    modules: ['src', 'node_modules'],
    moduleExtensions: ['-loader'],
  },
  resolve: {
    modules: ['src', 'node_modules'],
    descriptionFiles: ['package.json'],
    moduleExtensions: ['-loader'],
    extensions: ['.js', '.jsx', '.json'],
  },
};
