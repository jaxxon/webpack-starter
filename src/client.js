// @flow
import React from 'react';
import Dom from 'react-dom';

type Props = {
  firstName : string,
  lastName: string,
};

export const App = ({ firstName, lastName } : Props) => (
  <div>
    Start { firstName } - { lastName }
  </div>
);

Dom.render(
  <App firstName="Andreas" lastName="Jansson"  />,
  document.getElementById('root'),
);
